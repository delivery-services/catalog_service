package service

import (
	"context"
	"fmt"
	"log"
	pb "user_service/genproto/delivery-protos/catalog_service"
	"user_service/grpc/client"
	"user_service/storage"
)

type categoryService struct {
	storage  storage.IStorage
	services client.IServiceManager
	pb.UnimplementedCategoryServiceServer
}

func NewCategoryService(storage storage.IStorage, services client.IServiceManager) *categoryService {
	return &categoryService{
		storage:  storage,
		services: services,
	}
}

func (c *categoryService) Create(ctx context.Context, request *pb.CreateCategory) (*pb.Category, error) {
	category, err := c.storage.Category().Create(ctx, request)
	if err != nil {
		log.Println("error in category service in service package while creating category")
		return nil, err
	}
	

	return category, nil
}

func (c *categoryService) GetByID(ctx context.Context, request *pb.GetCategoryByID) (*pb.Category, error) {
	category, err := c.storage.Category().GetByID(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while getting category by id")
		return nil, err
	}
	return category, nil
}

func (c *categoryService) GetAll(ctx context.Context, request *pb.GetCategoryList) (*pb.CategoriesResponse, error) {
	categories, err := c.storage.Category().GetAll(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while getting category list")
		return nil, err
	}
	return categories, nil
}

func (c *categoryService) Update(ctx context.Context, request *pb.UpdateCategory) (*pb.CategoryResponse, error) {
	response, err := c.storage.Category().Update(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while updating category by id")
		return nil, err
	}
	return response, nil
}

func (c *categoryService) Delete(ctx context.Context, request *pb.GetCategoryByID) (*pb.CategoryResponse, error) {
	response, err := c.storage.Category().Delete(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while deleting category by id")
		return nil, err
	}
	return response, nil
}
