package service

import (
	"context"
	"fmt"
	"log"
	pb "user_service/genproto/delivery-protos/catalog_service"
	"user_service/grpc/client"
	"user_service/storage"
)

type productService struct {
	storage  storage.IStorage
	services client.IServiceManager
	pb.UnimplementedProductServiceServer
}

func NewProductService(storage storage.IStorage, services client.IServiceManager) *productService {
	return &productService{
		storage:  storage,
		services: services,
	}
}

func (p *productService) Create(ctx context.Context, request *pb.CreateProduct) (*pb.Product, error) {
	product, err := p.storage.Product().Create(ctx, request)
	if err != nil {
		log.Println("error in product service in service package while creating product")
		return nil, err
	}
	

	return product, nil
}

func (p *productService) GetByID(ctx context.Context, request *pb.GetProductByID) (*pb.Product, error) {
	product, err := p.storage.Product().GetByID(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while getting product by id")
		return nil, err
	}
	return product, nil
}

func (p *productService) GetAll(ctx context.Context, request *pb.GetProductList) (*pb.ProductsResponse, error) {
	products, err := p.storage.Product().GetAll(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while getting products list")
		return nil, err
	}
	return products, nil
}

func (p *productService) Update(ctx context.Context, request *pb.UpdateProduct) (*pb.ProductResponse, error) {
	response, err := p.storage.Product().Update(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while updating product by id")
		return nil, err
	}
	return response, nil
}

func (p *productService) Delete(ctx context.Context, request *pb.GetProductByID) (*pb.ProductResponse, error) {
	response, err := p.storage.Product().Delete(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while deleting product by id")
		return nil, err
	}
	return response, nil
}
