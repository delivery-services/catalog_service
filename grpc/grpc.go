package grpc

import (
	"user_service/genproto/delivery-protos/catalog_service"
	"user_service/grpc/client"
	"user_service/service"
	"user_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(storage storage.IStorage, services client.IServiceManager) *grpc.Server {
	grpcServer := grpc.NewServer()

	catalog_service.RegisterCategoryServiceServer(grpcServer, service.NewCategoryService(storage, services))
	catalog_service.RegisterProductServiceServer(grpcServer, service.NewProductService(storage,services))

	reflection.Register(grpcServer)

	return grpcServer

}
