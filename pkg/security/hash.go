package security

import "golang.org/x/crypto/bcrypt"

const (
	A2IDTime = 3

	A2IDMemory = 64 * 1024

	A2IDThreads = 4

	A2IDKeyLen = 32

	A2IDSaltLen = 16
)

func HashPassword(password string) (string, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return string(hashedPassword), nil
}

func CompareHashAndPassword(hashedPassword, password string) bool {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password)) == nil
}
