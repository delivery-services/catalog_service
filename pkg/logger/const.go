package logger

const (
	LevelDebug = "debug"

	LevelInfo = "info"

	LevelWarn = "warn"

	LevelError = "error"

	LevelDPanic = "dpanic"

	LevelPanic = "panic"

	LevelFatal = "fatal"
)
