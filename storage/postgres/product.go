package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	pb "user_service/genproto/delivery-protos/catalog_service"
	"user_service/pkg/logger"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type productRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
	pb.UnimplementedProductServiceServer
}

func NewProductRepo(db *pgxpool.Pool, log logger.ILogger) *productRepo {
	return &productRepo{
		db:  db,
		log: log,
	}
}

func (p *productRepo) Create(ctx context.Context, request *pb.CreateProduct) (*pb.Product, error) {

	var response = pb.Product{}
	var updatedAt = sql.NullString{}

	id := uuid.New()

	query := `insert into product (
		id, 
		title,
		description,
		photo,
		active,
		type,
		price,
		category_id
	 ) values ($1, $2, $3, $4, $5, $6, $7, $8)
	   returning
	    id, 
		title,
		description,
		photo,
		active,
		type,
		price,
		category_id,
		created_at::text,
		updated_at::text
	 `

	if err := p.db.QueryRow(ctx, query,
		id,
		request.Title,
		request.Description,
		request.Photo,
		request.Active,
		request.Type,
		request.Price,
		request.CategoryId,
	).Scan(
		&response.Id,
		&response.Title,
		&response.Description,
		&response.Photo,
		&response.Active,
		&response.Type,
		&response.Price,
		&response.CategoryId,
		&response.CreatedAt,
		&updatedAt,
	); err != nil {
		p.log.Error("error in catalog service while inserting and getting category")
		return nil, err
	}

	if updatedAt.Valid {
		response.UpdatedAt = updatedAt.String
	}

	return &response, nil

}

func (p *productRepo) GetByID(ctx context.Context, pKey *pb.GetProductByID) (*pb.Product, error) {

	var updatedAt = sql.NullString{}

	product := &pb.Product{}

	query := `select 
	    id, 
		title,
		description,
		photo,
		active,
		type,
		price,
		category_id,
	    created_at::text, 
	    updated_at::text 
	 from product where deleted_at = 0 and id = $1`

	row := p.db.QueryRow(ctx, query, pKey.Id)

	err := row.Scan(
		&product.Id,
		&product.Title,
		&product.Description,
		&product.Photo,
		&product.Active,
		&product.Type,
		&product.Price,
		&product.CategoryId,
		&product.CreatedAt,
		&updatedAt,
	)

	if err != nil {
		log.Println("error while selecting product", err.Error())
		return &pb.Product{}, err
	}

	if updatedAt.Valid {
		product.UpdatedAt = updatedAt.String
	}

	return product, nil

}

func (p *productRepo) GetAll(ctx context.Context, request *pb.GetProductList) (*pb.ProductsResponse, error) {
	var (
		products          = []*pb.Product{}
		count             = 0
		query, countQuery string
		page              = request.Page
		offset            = (page - 1) * request.Limit
		search            = request.Search
		updatedAt         = sql.NullString{}
	)

	countQuery = `select count(1) from product where deleted_at = 0 `

	if search != "" {
		countQuery += fmt.Sprintf(` and title ilike '%%%s%%'`, search)
	}
	if err := p.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		p.log.Error("error is while selecting client count", logger.Error(err))
		return &pb.ProductsResponse{}, err
	}

	query = `select 
	id, 
	title,
	description,
	photo,
	active,
	type,
	price,
	category_id,
	created_at::text, 
	updated_at::text 
	from product where deleted_at = 0 `

	if search != "" {
		query += fmt.Sprintf(` and title ilike '%%%s%%'`, search)
	}

	query += ` order by created_at desc LIMIT $1 OFFSET $2`
	rows, err := p.db.Query(ctx, query, request.Limit, offset)
	if err != nil {
		p.log.Error("error is while selecting product", logger.Error(err))
		return &pb.ProductsResponse{}, err
	}

	defer rows.Close()

	for rows.Next() {
		product := pb.Product{}
		if err = rows.Scan(
			&product.Id,
			&product.Title,
			&product.Description,
			&product.Photo,
			&product.Active,
			&product.Type,
			&product.Price,
			&product.CategoryId,
			&product.CreatedAt,
			&updatedAt); err != nil {
			p.log.Error("error is while scanning product data", logger.Error(err))
			return &pb.ProductsResponse{}, err
		}

		if updatedAt.Valid {
			product.UpdatedAt = updatedAt.String
		}

		products = append(products, &product)

	}

	if err := rows.Err(); err != nil {
		p.log.Error("error while iterating rows", logger.Error(err))
		return &pb.ProductsResponse{}, err
	}

	return &pb.ProductsResponse{
		Product: products,
		Count:   int32(count),
	}, nil
}

func (p *productRepo) Update(ctx context.Context, request *pb.UpdateProduct) (*pb.ProductResponse, error) {
	query := `
		update product set 
		title = $1, 
		description = $2,
        photo = $3,
		active = $4,
		type = $5,
		price = $6,
		category_id = $7,
		updated_at = now()
				where id = $8`

	if _, err := p.db.Exec(ctx, query,
		request.Title,
		request.Description,
		request.Photo,
		request.Active,
		request.Type,
		request.Price,
		request.CategoryId,
		request.Id); err != nil {
		fmt.Println("error while updating product data", err.Error())
		return &pb.ProductResponse{}, err
	}

	return &pb.ProductResponse{
		Response: "data succesfully updated",
	}, nil
}

func (p *productRepo) Delete(ctx context.Context, request *pb.GetProductByID) (*pb.ProductResponse, error) {

	query := `update product set deleted_at = extract(epoch from current_timestamp) where id = $1`

	if _, err := p.db.Exec(ctx, query, request.Id); err != nil {
		fmt.Println("error while deleting product by id", err.Error())
		return nil, err
	}

	return &pb.ProductResponse{
		Response: "data succesfully deleted",
	}, nil
}
