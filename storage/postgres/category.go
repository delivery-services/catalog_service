package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	pb "user_service/genproto/delivery-protos/catalog_service"
	"user_service/pkg/logger"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type categoryRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
	pb.UnimplementedCategoryServiceServer
}

func NewCategoryRepo(db *pgxpool.Pool, log logger.ILogger) *categoryRepo {
	return &categoryRepo{
		db:  db,
		log: log,
	}
}

func (c *categoryRepo) Create(ctx context.Context, request *pb.CreateCategory) (*pb.Category, error) {

	var response = pb.Category{}
	var updatedAt = sql.NullString{}

	id := uuid.New()

	query := `insert into category (
		id, 
		title,
		image,
		active,
		category_id
	 ) values ($1, $2, $3, $4, $5)
	   returning
	    id, 
		title,
		image,
		active,
		category_id,
		created_at::text,
		updated_at::text
	 `

	if err := c.db.QueryRow(ctx, query,
		id,
		request.Title,
		request.Image,
		request.Active,
		request.CategoryId,
	).Scan(
		&response.Id,
		&response.Title,
		&response.Image,
		&response.Active,
		&response.CategoryId,
		&response.CreatedAt,
		&updatedAt,
	); err != nil {
		c.log.Error("error in catalog service while inserting and getting category")
		return nil, err
	}

	if updatedAt.Valid {
		response.UpdatedAt = updatedAt.String
	}

	return &response, nil

}

func (c *categoryRepo) GetByID(ctx context.Context, pKey *pb.GetCategoryByID) (*pb.Category, error) {

	var updatedAt = sql.NullString{}

	category := &pb.Category{}

	query := `select 
	    id, 
		title,
		image,
		active,
		category_id,
	    created_at::text, 
	    updated_at::text 
	 from category where deleted_at = 0 and id = $1`

	row := c.db.QueryRow(ctx, query, pKey.Id)

	err := row.Scan(
		&category.Id,
		&category.Title,
		&category.Image,
		&category.Active,
		&category.CategoryId,
		&category.CreatedAt,
		&updatedAt,
	)

	if err != nil {
		log.Println("error while selecting client", err.Error())
		return &pb.Category{}, err
	}

	if updatedAt.Valid {
		category.UpdatedAt = updatedAt.String
	}

	return category, nil

}

func (c *categoryRepo) GetAll(ctx context.Context, request *pb.GetCategoryList) (*pb.CategoriesResponse, error) {
	var (
		categories            = []*pb.Category{}
		count                 = 0
		query, countQuery     string
		page                  = request.Page
		offset                = (page - 1) * request.Limit
		search                = request.Search
		updatedAt, categoryId = sql.NullString{}, sql.NullString{}
	)

	countQuery = `select count(1) from category where deleted_at = 0 `

	if search != "" {
		countQuery += fmt.Sprintf(` and active ilike '%%%s%%'`, search)
	}
	if err := c.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		c.log.Error("error is while selecting client count", logger.Error(err))
		return &pb.CategoriesResponse{}, err
	}

	query = `select 
	    id, 
		title,
		image,
		active,
		category_id,
	    created_at::text, 
	    updated_at::text 
		from category where deleted_at = 0 `

	if search != "" {
		query += fmt.Sprintf(` and active ilike '%%%s%%'`, search)
	}

	query += ` order by created_at desc LIMIT $1 OFFSET $2`
	rows, err := c.db.Query(ctx, query, request.Limit, offset)
	if err != nil {
		c.log.Error("error is while selecting category", logger.Error(err))
		return &pb.CategoriesResponse{}, err
	}

	defer rows.Close()

	for rows.Next() {
		category := pb.Category{}
		if err = rows.Scan(
			&category.Id,
			&category.Title,
			&category.Image,
			&category.Active,
			&categoryId,
			&category.CreatedAt,
			&updatedAt); err != nil {
			c.log.Error("error is while scanning client data", logger.Error(err))
			return &pb.CategoriesResponse{}, err
		}

		if updatedAt.Valid {
			category.UpdatedAt = updatedAt.String
		}

		if categoryId.Valid {
			category.CategoryId = categoryId.String
		}

		categories = append(categories, &category)

	}

	if err := rows.Err(); err != nil {
		c.log.Error("error while iterating rows", logger.Error(err))
		return &pb.CategoriesResponse{}, err
	}

	return &pb.CategoriesResponse{
		Category: categories,
		Count:    int32(count),
	}, nil
}

func (c *categoryRepo) Update(ctx context.Context, request *pb.UpdateCategory) (*pb.CategoryResponse, error) {
	query := `
		update category set 
		title = $1, 
		image = $2,
        active = $3,
		category_id = $4,
		updated_at = now()
				where id = $5`

	if _, err := c.db.Exec(ctx, query,
		request.Title,
		request.Image,
		request.Active,
		request.CategoryId,
		request.Id); err != nil {
		fmt.Println("error while updating category data", err.Error())
		return &pb.CategoryResponse{}, err
	}

	return &pb.CategoryResponse{
		Response: "data succesfully updated",
	}, nil
}

func (c *categoryRepo) Delete(ctx context.Context, request *pb.GetCategoryByID) (*pb.CategoryResponse, error) {

	query := `update category set deleted_at = extract(epoch from current_timestamp) where id = $1`

	if _, err := c.db.Exec(ctx, query, request.Id); err != nil {
		fmt.Println("error while deleting category by id", err.Error())
		return nil, err
	}

	return &pb.CategoryResponse{
		Response: "data succesfully deleted",
	}, nil
}
