package storage

import (
	"context"
	pb "user_service/genproto/delivery-protos/catalog_service"
)

type IStorage interface {
	Close()
	Category() ICategoryStorage
	Product() IProductStorage
}

type ICategoryStorage interface {
	Create(context.Context, *pb.CreateCategory) (*pb.Category, error)
	GetByID(context.Context, *pb.GetCategoryByID) (*pb.Category, error)
	GetAll(context.Context, *pb.GetCategoryList) (*pb.CategoriesResponse, error)
	Update(context.Context, *pb.UpdateCategory) (*pb.CategoryResponse, error)
	Delete(context.Context, *pb.GetCategoryByID) (*pb.CategoryResponse, error)
}

type IProductStorage interface {
	Create(context.Context, *pb.CreateProduct) (*pb.Product, error)
	GetByID(context.Context, *pb.GetProductByID) (*pb.Product, error)
	GetAll(context.Context, *pb.GetProductList) (*pb.ProductsResponse, error)
	Update(context.Context, *pb.UpdateProduct) (*pb.ProductResponse, error)
	Delete(context.Context, *pb.GetProductByID) (*pb.ProductResponse, error)
}
